package com.example.suhai.opensooqtask.presenter;

import android.util.Log;

import com.example.suhai.opensooqtask.model.IWeatherResponse;
import com.example.suhai.opensooqtask.model.WeatherResponse;
import com.example.suhai.opensooqtask.retrofit.IMethodsToRequest;
import com.example.suhai.opensooqtask.retrofit.ServerManager;
import com.example.suhai.opensooqtask.view.IWeatherView;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Retrofit;

public class WeatherPresenter implements IWeatherPresenter {


    IWeatherView weatherView;

    Realm realm;

    IMethodsToRequest methodsToRequest;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public WeatherPresenter(IWeatherView weatherView) {
        this.weatherView = weatherView;
        Retrofit retrofit = ServerManager.getInstance();
        methodsToRequest = retrofit.create(IMethodsToRequest.class);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onDataRequest(String name) {


        compositeDisposable.add(methodsToRequest.getWeather(ServerManager.API_KEY, name, 5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<WeatherResponse>() {
                    @Override
                    public void onNext(WeatherResponse weatherResponse) {
                        weatherResponse.setName(name);
                        saveToRealm(weatherResponse);
                        weatherView.onWeatherRequestResult(true,weatherResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadFromCached(name);

                    }

                    @Override
                    public void onComplete() {

                    }
                }));


    }

    private void loadFromCached(String name) {

        RealmResults<WeatherResponse> result = realm.where(WeatherResponse.class)
                .equalTo("name", name)
                .findAll();
        if(!result.isEmpty()) {
            weatherView.onWeatherRequestResult(false, result.get(0));
        }
    }

    private void saveToRealm(WeatherResponse weatherResponse) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {

                bgRealm.copyToRealmOrUpdate(weatherResponse);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                // Transaction was a success.
                Log.d("Realm","---------------success---------------");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                Log.e("Realm",error.getMessage(),error);
            }
        });
    }


    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }


    public void closeRealm(){

        realm.close();
    }
}
