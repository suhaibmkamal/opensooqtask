package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.Observable;

import io.realm.RealmObject;

public class Current extends RealmObject implements ICurrent ,Serializable {


    double temp_c;
    Condition condition;
    double humidity;
    double cloud;
    double feelslike_c;
    double wind_kph;

    @Override
    public double getCTemp() {
        return temp_c;
    }

    @Override
    public Condition getCondition() {
        return condition;
    }

    @Override
    public double getHumidity() {
        return humidity;
    }

    @Override
    public double getCloud() {
        return cloud;
    }

    @Override
    public double getWindKPH() {
        return wind_kph;
    }

    @Override
    public double getFeelsLikeC() {
        return feelslike_c;
    }
}
