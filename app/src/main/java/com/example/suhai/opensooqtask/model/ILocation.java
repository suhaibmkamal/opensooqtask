package com.example.suhai.opensooqtask.model;

public interface ILocation {
    String getName();
    String getRegion();
    String getCountry();
    String getLocalDate();
    double getLat();
    double getLon();
}
