package com.example.suhai.opensooqtask.model;

public interface IDay {

    double getMaxTemp();
    double getMinTemp();
    Condition getCondition();


}
