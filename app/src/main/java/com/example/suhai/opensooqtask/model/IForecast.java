package com.example.suhai.opensooqtask.model;

import java.util.ArrayList;

import io.realm.RealmList;

public interface IForecast {

    RealmList<ForecastDay> getDays();
}
