package com.example.suhai.opensooqtask.retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerManager {

    public final static String API_KEY = "3cd86843750d462cbd0212916182411";
    private static  Retrofit ourInstance;

    public static synchronized Retrofit getInstance() {
        if (ourInstance == null){

            ourInstance = new Retrofit.Builder()
                    .baseUrl("http://api.apixu.com/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            return ourInstance;
        }else {
            return ourInstance;
        }
    }

    private ServerManager() {
    }
}
