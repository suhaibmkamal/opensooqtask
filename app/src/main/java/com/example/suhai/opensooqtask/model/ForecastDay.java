package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import io.realm.RealmObject;

public class ForecastDay extends RealmObject implements IForecastDay ,Serializable {

    Day day;

    @Override
    public Day getDay() {
        return day;
    }
}
