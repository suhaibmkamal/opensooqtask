package com.example.suhai.opensooqtask.view;

import com.example.suhai.opensooqtask.model.WeatherResponse;

public interface IWeatherView {

    void onWeatherRequestResult(boolean isSuccess, WeatherResponse weatherResponse);
}
