package com.example.suhai.opensooqtask.model;

public interface IWeatherResponse {
    Location getLocation();
    Current getCurrent();
    Forecast getForecast();
    String getName();
    void  setName(String name);
}
