package com.example.suhai.opensooqtask.presenter;

public interface IWeatherPresenter {
    void onDataRequest(String name);
}
