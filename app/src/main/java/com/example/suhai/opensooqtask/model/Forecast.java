package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Forecast extends RealmObject implements IForecast,Serializable {
    RealmList<ForecastDay> forecastday;

    @Override
    public RealmList<ForecastDay> getDays() {
        return forecastday;
    }

}
