package com.example.suhai.opensooqtask.retrofit;



import com.example.suhai.opensooqtask.model.WeatherResponse;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IMethodsToRequest {

    @GET("forecast.json")
    Observable<WeatherResponse> getWeather(@Query("key")String key, @Query("q") String name, @Query("days") int days);
}
