package com.example.suhai.opensooqtask.model;

public interface ICurrent {
    double getCTemp();
    Condition getCondition();
    double getHumidity();
    double getCloud();
    double getWindKPH();
    double getFeelsLikeC();
}
