package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.Observable;

import io.realm.RealmObject;

public class Location extends RealmObject implements ILocation , Serializable {

    /*"name": "Amman",
    "region": "Amman Governorate",
    "country": "Jordan",
    "lat": 31.95,
    "lon": 35.93,
    "tz_id": "Asia/Amman",
    "localtime_epoch": 1543177818,
    "localtime": "2018-11-25 22:30"*/

    String name;
    String region;
    String country;
    String localtime;
    double lat;
    double lon;
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRegion() {
        return region;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public String getLocalDate() {
        return localtime;
    }

    @Override
    public double getLat() {
        return lat;
    }

    @Override
    public double getLon() {
        return lon;
    }
}
