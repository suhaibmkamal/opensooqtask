package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.Observable;

import io.reactivex.disposables.Disposable;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WeatherResponse extends RealmObject implements IWeatherResponse, Serializable,Disposable {

    @PrimaryKey
    String name;
    Location location;
    Current current;
    Forecast forecast;
    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public Current getCurrent() {
        return current;
    }

    @Override
    public Forecast getForecast() {
        return forecast;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {

        this.name = name;
    }

    @Override
    public void dispose() {

    }

    @Override
    public boolean isDisposed() {
        return false;
    }
}
