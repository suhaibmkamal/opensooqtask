package com.example.suhai.opensooqtask.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.suhai.opensooqtask.R;
import com.example.suhai.opensooqtask.model.WeatherResponse;
import com.example.suhai.opensooqtask.presenter.WeatherPresenter;
import com.example.suhai.opensooqtask.view.IWeatherView;

import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements IWeatherView {

    WeatherPresenter weatherPresenter;
    ImageView ivConditionImage;
    TextView tvCity;
    TextView tvCountry;
    TextView tvDegree;
    TextView tvConditionText;
    TextView tvHumidity;
    TextView tvCloud;
    TextView tvSpeed;
    TextView tvFeelsLike;
    TextView tvDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        weatherPresenter = new WeatherPresenter(this);
        weatherPresenter.onDataRequest("Amman");

    }

    private void initViews() {
        ivConditionImage = findViewById(R.id.ivConditionImage);
        tvCity = findViewById(R.id.tvCity);
        tvCountry = findViewById(R.id.tvCountry);
        tvDegree = findViewById(R.id.tvDegree);
        tvConditionText = findViewById(R.id.tvConditionText);
        tvDate = findViewById(R.id.tvDate);
        tvHumidity = findViewById(R.id.tvHumidity);
        tvFeelsLike = findViewById(R.id.tvFeelsLike);
        tvCloud = findViewById(R.id.tvCloud);
        tvSpeed = findViewById(R.id.tvSpeed);
    }

    @Override
    public void onWeatherRequestResult(boolean isSuccess, WeatherResponse weatherResponse) {

        if(isSuccess){
            if(weatherResponse!= null){
              loadData(weatherResponse);
            }

        }else{

            if(weatherResponse!= null){
                loadData(weatherResponse);
            }

            Toast.makeText(this,"something went wrong the data from cached",Toast.LENGTH_SHORT).show();
        }

    }

    private void loadData(WeatherResponse weatherResponse) {
        Glide.with(this).load("http:"+weatherResponse.getCurrent().getCondition().getIcon()).into(ivConditionImage);
        tvCity.setText(weatherResponse.getLocation().getName());
        tvCountry.setText(String.format("%s%s", getResources().getString(R.string.comma), weatherResponse.getLocation().getCountry()));
        tvDegree.setText(String.format("%s %s", String.valueOf(weatherResponse.getCurrent().getCTemp()), getResources().getString(R.string.c)));
        tvDate.setText(weatherResponse.getLocation().getLocalDate());
        tvHumidity.setText(String.valueOf(weatherResponse.getCurrent().getHumidity()));
        tvCloud.setText(String.valueOf(weatherResponse.getCurrent().getCloud()));
        tvFeelsLike.setText(String.valueOf(weatherResponse.getCurrent().getFeelsLikeC()));
        tvSpeed.setText(String.valueOf(weatherResponse.getCurrent().getWindKPH()));
        tvConditionText.setText(weatherResponse.getCurrent().getCondition().getText());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.irbidItem:
                weatherPresenter.onDataRequest("Irbid");
                break;

            case R.id.aqabaItem:
                weatherPresenter.onDataRequest("Aqaba");
                break;
            case R.id.ammanItem:
                weatherPresenter.onDataRequest("Amman");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        weatherPresenter.clearCompositeDisposable();
        super.onStop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        weatherPresenter.closeRealm();
        super.onDestroy();
    }
}
