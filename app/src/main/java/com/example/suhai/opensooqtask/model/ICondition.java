package com.example.suhai.opensooqtask.model;

public interface ICondition {

    String getText();
    String getIcon();
}
