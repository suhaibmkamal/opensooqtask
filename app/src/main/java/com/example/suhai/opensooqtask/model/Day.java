package com.example.suhai.opensooqtask.model;

import java.io.Serializable;

import io.realm.RealmObject;

public class Day extends RealmObject implements IDay ,Serializable {

    double maxtemp_c;
    double mintemp_c;
    Condition condition;
    @Override
    public double getMaxTemp() {
        return maxtemp_c;
    }

    @Override
    public double getMinTemp() {
        return mintemp_c;
    }

    @Override
    public Condition getCondition() {
        return condition;
    }
}
