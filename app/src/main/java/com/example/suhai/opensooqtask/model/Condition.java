package com.example.suhai.opensooqtask.model;

import java.io.Serializable;
import java.util.Observable;

import io.realm.RealmObject;

public class Condition extends RealmObject implements ICondition , Serializable {

    String text;
    String icon;
    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getIcon() {
        return icon;
    }
}
